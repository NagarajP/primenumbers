package com.myzee.prime;

public class FindPrimeNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPrimeNumber(31));
	}
	
	private static boolean isPrimeNumber(int n) {
		if(n == 0 || n == 1) {
			return true;
		}
		
		for(int i = 2; i < n/2; i++) {
			if(n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
