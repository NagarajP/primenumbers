package com.myzee.prime;


/*
 * Find 'n' prime numbers from 'initial' number.
 * Ex, find 5 prime numbers after 115.
 * 
 * 127
 * 131
 * 137
 * 139
 * 149
 */
public class PrimenumberInRange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		findPrimeInRange(115, 5);
	}
	
	public static void findPrimeInRange(int initial, int n) {
		int count = 0, flag = 0;
		while(initial != -1) {
			int halfOfPrimeNumber = initial/2;
			for(int i = 2; i <= halfOfPrimeNumber; i++) {
				if(initial % i == 0) {
					flag = 0;
					break;
				} else {
					flag = 1;
				}
			}
			
			// if flag=1, it means the number is prime so increase the count. So you found one more prime number.
			if(flag == 1 ) {
				System.out.println(initial);
				count++;
				flag = 0;
				if(count == n) {
					break;
				}
			}
			initial++;
		}
	}
}
